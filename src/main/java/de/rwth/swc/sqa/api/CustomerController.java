package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.model.DiscountCard.ValidForEnum;
import de.rwth.swc.sqa.services.CustomerService;
import de.rwth.swc.sqa.services.DiscountCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Controller
public class CustomerController implements CustomersApi {
    private static long idCounter = 0;
    private static long idCounterDiscountCard = 0;

    private static synchronized long createCustomerId() {
        return idCounter++;
    }

    private static synchronized long createDiscountCardId() {
        return idCounterDiscountCard++;
    }

    @Autowired
    CustomerService customerService;
    @Autowired
    DiscountCardService discountCardService;

    @Override
    public ResponseEntity<Customer> addCustomer(final Customer body) {
        if (body.getId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // check if birthdate is valid trough parsing
        try {
            LocalDate.parse(body.getBirthdate(), DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (body.getDisabled() == null) {
            body.setDisabled(false);
        }
        final long customerId = createCustomerId();
        body.setId(customerId);
        customerService.addCustomer(body);

        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(Long customerId, DiscountCard body) {
        List<Long> customerIdList = customerService.getCustomerList().stream().map(Customer::getId).toList();

        // return Bad_Request if an id is provided
        if (body.getId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // check if both custumer ids are equal
        if (!body.getCustomerId().equals(customerId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // check if customer exists
        if (!customerIdList.contains(customerId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // check if Type is correct
        if (body.getType() != 25 && body.getType() != 50) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // check if date is valid
        LocalDate startDate1;
        try {
            startDate1 = LocalDate.parse(body.getValidFrom(), DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // if multiple cards exist, check for collision
        List<DiscountCard> getCustomerDiscountCardList = discountCardService.getDiscountCardList().stream()
                .filter(a -> a.getCustomerId().equals(customerId)).toList();

        if (!getCustomerDiscountCardList.isEmpty()) {
            for (DiscountCard dc : getCustomerDiscountCardList) {
                try {
                    if (overlapDate(startDate1, body.getValidFor(), dc)) {
                        return new ResponseEntity<>(HttpStatus.CONFLICT);
                    }
                } catch (DateTimeParseException e) {
                    // ParseException is never reached, as the catch earlier already parses this
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            }
        }

        final long id = createDiscountCardId();
        body.setId(id);
        discountCardService.addDiscountCard(body);

        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {
        /*
         *
         * Getting the list of discount codes and filtering them based on the customerId
         * then filtering them based on Valid From in descending order
         *
         * The commented code implements the sorting in ascending order of Valid From
         * dates
         */
        List<DiscountCard> getCustomerDiscountCardList = discountCardService.getDiscountCardList().stream()
                .filter(a -> a.getCustomerId().equals(customerId))
                .sorted((o1, o2) -> o2.getValidFrom().compareTo(o1.getValidFrom())).toList();

        /*
         * Return the responses in a manner that
         * if there is at least 1 discount: return the list of discount codes for that
         * customer ID
         * else return no customer ir discount code found associated,
         * this response can be modified later based on user existence and discount
         * codes existence
         */
        return getCustomerDiscountCardList.isEmpty() ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                : new ResponseEntity<>(getCustomerDiscountCardList, HttpStatus.OK);

    }

    static boolean overlapDate(LocalDate sD1, DiscountCard.ValidForEnum validFor1, DiscountCard d2)
            throws DateTimeParseException {
        LocalDate sD2;
        sD2 = LocalDate.parse(d2.getValidFrom(), DateTimeFormatter.ISO_LOCAL_DATE);

        LocalDate eD1;
        LocalDate eD2;

        if (validFor1 == ValidForEnum._1Y) {
            eD1 = sD1.plusYears(1).minusDays(1);
        } else {
            eD1 = sD1.plusDays(29);
        }
        if (d2.getValidFor() == ValidForEnum._1Y) {
            eD2 = sD2.plusYears(1).minusDays(1);
        } else {
            eD2 = sD2.plusDays(29);
        }

        return (sD1.isBefore(eD2) || sD1.isEqual(eD2)) && (sD2.isBefore(eD1) || sD2.isEqual(eD1));
    }
}
