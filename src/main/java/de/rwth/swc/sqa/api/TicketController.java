package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import de.rwth.swc.sqa.services.CustomerService;
import de.rwth.swc.sqa.services.DiscountCardService;
import de.rwth.swc.sqa.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;


@Controller
public class TicketController implements TicketsApi {

    @Autowired
    CustomerService customerService;
    @Autowired
    DiscountCardService discountCardService;
    @Autowired
    TicketService ticketService;
    private final Clock clock;

    TicketController(Clock clock){
        this.clock = clock;
    }

    long ticketIDCounter = 0;

    @Override
    public ResponseEntity<Ticket> buyTicket(TicketRequest body) {
        Ticket ticket = new Ticket();

        // Default value for disabled and student if not set
        ticket.setDisabled(body.getDisabled());
        if (body.getDisabled() == null){
            ticket.setDisabled(false);
        }

        ticket.setStudent(body.getStudent());
        if (body.getStudent() == null){
            ticket.setStudent(false);
        }

        // Check if discount card has a valid value
        ticket.setDiscountCard(body.getDiscountCard());
        if (!discountCardCheck(ticket)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // check if birthdate is valid trough parsing
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sdf.parse(body.getBirthdate());
        }catch (ParseException e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        // Retrieve birthdate from body for ticket
        ticket.setBirthdate(body.getBirthdate());

        // check if validFrom is valid trough parsing
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        try {
            sdf2.parse(body.getValidFrom());
        }catch (ParseException e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        // Retrieve validFrom from body for ticket
        ticket.setValidFrom(body.getValidFrom());
        if(!validFromInFutureCheck(ticket)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // Retrieve valid time from body for ticket
        boolean requiresZone;
        switch (body.getValidFor()) {
            case _1H -> {
                ticket.setValidFor(Ticket.ValidForEnum._1H);
                requiresZone = true;
            }
            case _1D -> {
                ticket.setValidFor(Ticket.ValidForEnum._1D);
                requiresZone = false;
            }
            case _30D -> {
                ticket.setValidFor(Ticket.ValidForEnum._30D);
                requiresZone = false;
            }
            default -> { //case 1 year
                ticket.setValidFor(Ticket.ValidForEnum._1Y);
                requiresZone = false;
            }
        }

        // Retrieve zone from body for ticket
        if (!zoneCheck(body, requiresZone)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (requiresZone){
            switch (body.getZone()) {
                case A -> ticket.setZone(Ticket.ZoneEnum.A);
                case B -> ticket.setZone(Ticket.ZoneEnum.B);
                default -> ticket.setZone(Ticket.ZoneEnum.C);
            }
        }

        if (!studentValidForCheck(ticket)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //Save the ticket
        ticketIDCounter++;
        ticket.setId(ticketIDCounter);

        ticketService.addTicket(ticket);
        return new ResponseEntity<>(ticket, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> validateTicket(TicketValidationRequest body) {
        // Register Clock to use for the ticket validation
        Instant instant = Instant.now(clock);
        // Check for ticketId in tickets Arraylist
        Ticket ticket = ticketService.getTicketFromId(body.getTicketId());
        if (ticket == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Check if ZoneEnum is included in the tickets Zone
        if (!checkForZone(body.getZone(), ticket)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Check if disabled is valid
        if (disabledValidationCheck(ticket.getDisabled(), body.getDisabled())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Check if student is valid
        if (studentValidationCheck(ticket.getStudent(), body.getStudent())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Check if discount card exists
        DiscountCard discountCard = checkForDiscountCard(body.getDiscountCardId());
        if (body.getDiscountCardId() != null && discountCard == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Parse all dates
        LocalDateTime cDate = LocalDateTime.ofInstant(instant, ZoneId.of("Europe/London"));
        LocalDateTime ticketDateValidFrom;
        LocalDate birthdate;
        LocalDate discountDateValidFrom = null;
        try {
            ticketDateValidFrom = LocalDateTime.parse(ticket.getValidFrom(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            birthdate = LocalDate.parse(ticket.getBirthdate(), DateTimeFormatter.ISO_LOCAL_DATE);
            if (discountCard != null){
                discountDateValidFrom = LocalDate.parse(discountCard.getValidFrom(), DateTimeFormatter.ISO_LOCAL_DATE);
            }
        } catch (DateTimeParseException e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // if buyer is student, check if is still student
        if (studentAgeValidation(birthdate, cDate.toLocalDate(), ticket.getStudent())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Check if ticket is valid on the body's date
        boolean validDateTicket;
        validDateTicket = checkValidDateTicket(cDate, ticketDateValidFrom, ticket.getValidFor());
        if (validDateTicket){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Check if discount card is valid on the body's date
        // Check if a discount card is used in the ticket and the Integer Values are equal
        if (discountCard != null){
            boolean validDateDiscountCard;
            if (!Objects.equals(ticket.getDiscountCard(), discountCard.getType())){
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            validDateDiscountCard = checkValidDateDiscountCard(cDate.toLocalDate() ,discountDateValidFrom, discountCard.getValidFor());
            if (validDateDiscountCard){
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private boolean discountCardCheck(Ticket ticket){
        return !(ticket.getDiscountCard() != null && (ticket.getDiscountCard() != 25  && ticket.getDiscountCard() != 50));
    }

    private boolean zoneCheck(TicketRequest body, boolean requiresZone){
        if (body.getZone() == null && requiresZone) {
            return false;
        }
        return body.getZone() == null || requiresZone;
    }

    private boolean validFromInFutureCheck(Ticket ticket){
        Instant instant = Instant.now(clock);
        LocalDateTime cDate = LocalDateTime.ofInstant(instant, ZoneId.of("Europe/London"));

        return ticket.getValidFrom().compareTo(cDate.toString()) > 0;
    }

    private boolean studentValidForCheck(Ticket ticket){
        // Student Tickets can only be booked for 30d or 1y
        return !(ticket.getStudent() && (ticket.getValidFor() == Ticket.ValidForEnum._1H || ticket.getValidFor() == Ticket.ValidForEnum._1D));
    }

    private boolean checkValidDateTicket(LocalDateTime cDate, LocalDateTime ticketDateValidFrom, Ticket.ValidForEnum validEnum){
        LocalDateTime ticketDateValidUntil = calculateTicketDateValidUntil(ticketDateValidFrom, validEnum);
        return !((ticketDateValidFrom.isBefore(cDate) || ticketDateValidFrom.isEqual(cDate)) && (cDate.isBefore(ticketDateValidUntil) || cDate.isEqual(ticketDateValidUntil)));
    }

    private boolean checkValidDateDiscountCard(LocalDate cDate, LocalDate discountDateValidFrom, DiscountCard.ValidForEnum validEnum){
        LocalDate discountDateValidUntil;
        // Calculate date values
        if (validEnum == DiscountCard.ValidForEnum._30D) {
            discountDateValidUntil = discountDateValidFrom.plusDays(29);
        } else { // 1 Year
            discountDateValidUntil = discountDateValidFrom.plusYears(1).minusDays(1);
        }
        return !((discountDateValidFrom.isBefore(cDate) || discountDateValidFrom.isEqual(cDate)) && (cDate.isBefore(discountDateValidUntil) || cDate.isEqual(discountDateValidUntil)));
    }

    private DiscountCard checkForDiscountCard(Long discountCardId){
        if (discountCardId == null){
            return null;
        }
        return discountCardService.getDiscountCardFromId(discountCardId);
    }

    private boolean checkForZone(TicketValidationRequest.ZoneEnum zoneToCheck, Ticket ticket){
        if (ticket.getZone() == null){
            return true;
        }
        if (ticket.getZone().equals(Ticket.ZoneEnum.A) && (zoneToCheck.equals(TicketValidationRequest.ZoneEnum.B) || zoneToCheck.equals(TicketValidationRequest.ZoneEnum.C))){
            return false;
        }
        return !ticket.getZone().equals(Ticket.ZoneEnum.B) || !zoneToCheck.equals(TicketValidationRequest.ZoneEnum.C);
    }

    private boolean disabledValidationCheck(Boolean ticketDisabled, Boolean ticketRequestDisabled){
        if (ticketDisabled.equals(ticketRequestDisabled)){
            return false;
        }
        return ticketDisabled;
    }

    private boolean studentValidationCheck(Boolean ticketStudent, Boolean ticketRequestStudent){
        // Basically the same implementation as disabledValidationCheck, but as this might change later, this has its own function
        if (ticketStudent.equals(ticketRequestStudent)){
            return false;
        }
        return ticketStudent;
    }

    private boolean studentAgeValidation(LocalDate studentBirthdate, LocalDate cDate, boolean isStudent){
        if (!isStudent){
            return false;
        }
        return cDate.minusYears(28).compareTo(studentBirthdate) > 0;
    }

    private LocalDateTime calculateTicketDateValidUntil(LocalDateTime ticketDateValidFrom, Ticket.ValidForEnum validEnum){
        // Calculate date values
        return switch (validEnum) {
            case _1H -> ticketDateValidFrom.plusHours(1);
            case _1D -> ticketDateValidFrom.plusDays(1);
            case _30D -> ticketDateValidFrom.plusDays(30);
            default -> ticketDateValidFrom.plusYears(1); // 1 year
        };
    }
}
