package de.rwth.swc.sqa.services;

import de.rwth.swc.sqa.model.DiscountCard;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DiscountCardServiceImpl implements DiscountCardService{
    private final List<DiscountCard> discountCardList = new ArrayList<>();

    @Override
    public void addDiscountCard(DiscountCard discountCard){
        discountCardList.add(discountCard);
    }

    @Override
    public List<DiscountCard> getDiscountCardList(){
        return  discountCardList;
    }

    @Override
    public DiscountCard getDiscountCardFromId(long discountCardId){
        for (DiscountCard dc : discountCardList){
            if (dc.getId().equals(discountCardId)){
                return dc;
            }
        }
        return null;
    }
}
