package de.rwth.swc.sqa.services;

import org.springframework.stereotype.Service;

import de.rwth.swc.sqa.model.Customer;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{
    private final List<Customer> customerList = new ArrayList<>();

    @Override
    public void addCustomer(Customer customer){
        customerList.add(customer);
    }

    @Override
    public List<Customer> getCustomerList(){
        return customerList;
    }
}
