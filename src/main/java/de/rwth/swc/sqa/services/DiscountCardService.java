package de.rwth.swc.sqa.services;

import java.util.List;

import de.rwth.swc.sqa.model.DiscountCard;

public interface DiscountCardService {
    public abstract void addDiscountCard(DiscountCard discountCard);
    public abstract List<DiscountCard> getDiscountCardList();
    public abstract DiscountCard getDiscountCardFromId(long discountCardId);
}
