package de.rwth.swc.sqa.services;

import de.rwth.swc.sqa.model.Ticket;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService{
    private final List<Ticket> ticketList = new ArrayList<>();

    @Override
    public void addTicket(Ticket ticket){
        ticketList.add(ticket);
    }

    @Override
    public Ticket getTicketFromId(long ticketId){
        for (Ticket ticket : ticketList) {
            if (ticket.getId().equals(ticketId)) {
                return ticket;
            }
        }
        return null;
    }
}
