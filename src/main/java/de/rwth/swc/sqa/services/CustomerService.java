package de.rwth.swc.sqa.services;

import de.rwth.swc.sqa.model.Customer;

import java.util.List;

public interface CustomerService {
    public abstract void addCustomer(Customer customer);
    public abstract List<Customer> getCustomerList();
}
