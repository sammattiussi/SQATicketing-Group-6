package de.rwth.swc.sqa.services;

import de.rwth.swc.sqa.model.Ticket;

public interface TicketService {
    public abstract void addTicket(Ticket ticket);
    public abstract Ticket getTicketFromId(long ticketId);
}
