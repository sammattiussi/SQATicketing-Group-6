package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.threeten.extra.MutableClock;

import java.util.HashSet;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class AddCustomerTest {
    private static final String PATH = "/customers";
    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @ParameterizedTest
    @ValueSource(longs = {-1,0,1, Long.MAX_VALUE})
    void addCustomer_shouldReturn400ForExistingId(long customerId){
        final String requestBody = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true, \"id\" : "+customerId+" }";
        given().header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    void addCustomer_shouldReturn201(){
        final String requestBody = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        given().header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(PATH)
                .then().assertThat().statusCode(201);
    }

    @Test
    void addCustomer_shouldReturnCorrectFormat(){
        final String requestBody = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        given().header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(PATH)
                .then().assertThat().body(containsString("birthdate"))
                .assertThat().body(containsString("disabled"))
                .assertThat().body(containsString("id"));
    }

    @Test
    void addCustomer_shouldReturn400ForMissingBirthdate(){
        final String requestBody = "{\"disabled\" : true }";
        given().header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    void addCustomer_shouldReturn400ForInvalidBirthdate(){
        final String requestBody = "{ \"birthdate\" : \"1992\", \"disabled\" : true }";
        given().header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    void addCustomer_shouldSetDisabledFalseForMissingDisabled(){
        final String requestBody = "{ \"birthdate\" : \"1992-01-01\" }";
        given().header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(PATH)
                .then().assertThat().body("disabled",equalTo(false));
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void addCustomer_shouldNotChangeGivenDisabled(boolean disabled){
        final String requestBody = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : "+disabled+" }";
        given().header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(PATH)
                .then().assertThat().body("disabled", equalTo(disabled));
    }

    @ParameterizedTest
    @ValueSource(ints = {10})
    void addCustomer_shouldReturnDifferentIds(int numberOfCustomers){
        final String requestBody = "{ \"birthdate\" : \"1992-01-01\" }";
        final HashSet<Long> customerIds = new HashSet<>();
        for (int i=0;i<numberOfCustomers;i++){
            final Long customerId = ((Number) given().header("Content-type", "application/json")
                    .and()
                    .body(requestBody)
                    .when()
                    .post(PATH)
                    .then().extract().path("id")).longValue();
            assertFalse(customerIds.contains(customerId));
            customerIds.add(customerId);
        }
    }
}