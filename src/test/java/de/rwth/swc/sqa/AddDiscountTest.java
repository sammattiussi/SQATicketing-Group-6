package de.rwth.swc.sqa;

import java.util.Map;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.path.json.JsonPath.from;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.threeten.extra.MutableClock;

import static org.junit.jupiter.api.Assertions.fail;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class AddDiscountTest {
        public static Response customerResponse;
        public Map<String, ?> customerId;

        @LocalServerPort
        private int port;

        @BeforeEach
        public void setUp() {
                RestAssured.port = port;
                String request = "{"
                                + "\"birthdate\": \"1992-01-01\""
                                + "}";
                customerResponse = given().header("Content-Type", "application/json")
                                .and()
                                .body(request)
                                .when()
                                .post("/customers")
                                .then().contentType(ContentType.JSON).extract().response();

                customerId = from(customerResponse.asString()).get("");
                if (customerId.getOrDefault("id", null) == null) {
                        fail("Customer did not return an id.");
                }

        }

        @Test
        void whenIdIsIncluded() {
                String request = "{"
                                + "\"id\": 0,"
                                + "\"customerId\": " + customerId.get("id") + ","
                                + "\"validFor\": \"30d\","
                                + "\"validFrom\": \"1992-01-01\","
                                + "\"type\": 25"
                                + "}";

                given().header("Content-Type", "application/json")
                                .and()
                                .body(request)
                                .when()
                                .post("/customers/{customerId}/discountcards", customerId.get("id"))
                                .then().statusCode(400);
        }

        @Test
        void whenCustomerIdNotFound() {
                String request = "{"
                                + "\"customerId\": " + Long.MAX_VALUE + ","
                                + "\"validFor\": \"30d\","
                                + "\"validFrom\": \"1992-01-01\","
                                + "\"type\": 25"
                                + "}";

                given().header("Content-Type", "application/json")
                                .and()
                                .body(request)
                                .when()
                                .post("/customers/{customerId}/discountcards", Long.MAX_VALUE)
                                .then().statusCode(404);
        }

        @Test
        void whenDiscountCardTypeInvalid() {
                String request = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"1999-06-01\","
                        + "\"type\": 1"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(400);
        }

        @Test
        void whenDiscountCardAddedSuccessfully() {
                String request = "{"
                                + "\"customerId\": " + customerId.get("id") + ","
                                + "\"validFor\": \"30d\","
                                + "\"validFrom\": \"1999-06-01\","
                                + "\"type\": 25"
                                + "}";

                given().header("Content-Type", "application/json")
                                .and()
                                .body(request)
                                .when()
                                .post("/customers/{customerId}/discountcards", customerId.get("id"))
                                .then().statusCode(201);
        }

        @Test
        void whenDiscountCardIdsDifferent() {
                String request = "{"
                        + "\"birthdate\": \"1992-01-01\""
                        + "}";
                customerResponse = given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers")
                        .then().contentType(ContentType.JSON).extract().response();

                Map<String, ?> customerId2 = from(customerResponse.asString()).get("");
                if (customerId2.getOrDefault("id", null) == null) {
                        fail("Customer did not return an id.");
                }

                request = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"1999-06-01\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId2.get("id"))
                        .then().statusCode(400);
        }

        @Test
        void whenDiscountCardDateInvalid() {
                String request = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"test\","
                        + "\"type\": 50"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(400);
        }

        @Test
        void whenDiscountCardsConflict30dEarlier() {
                String request2 = "{"
                                + "\"customerId\": " + customerId.get("id") + ","
                                + "\"validFor\": \"30d\","
                                + "\"validFrom\": \"1999-01-02\","
                                + "\"type\": 25"
                                + "}";

                given().header("Content-Type", "application/json")
                                .and()
                                .body(request2)
                                .when()
                                .post("/customers/{customerId}/discountcards", customerId.get("id"))
                                .then().statusCode(201);

                String request = "{"
                                + "\"customerId\": " + customerId.get("id") + ","
                                + "\"validFor\": \"30d\","
                                + "\"validFrom\": \"1999-01-01\","
                                + "\"type\": 25"
                                + "}";

                given().header("Content-Type", "application/json")
                                .and()
                                .body(request)
                                .when()
                                .post("/customers/{customerId}/discountcards", customerId.get("id"))
                                .then().statusCode(409);
        }

        @Test
        void whenDiscountCardsConflict30dLater() {
                String request2 = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"1999-01-01\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request2)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(201);

                String request = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"1999-01-20\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(409);
        }

        @Test
        void whenDiscountCardsNoConflict() {
                String request2 = "{"
                        + "\"customerId\":" + customerId.get("id") + ","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"1999-01-01\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request2)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(201);

                String request = "{"
                        + "\"customerId\":" + customerId.get("id") + ","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"1996-01-20\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(201);
        }

        @Test
        void whenDiscountCardsConflict1YEarlier() {
                String request2 = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"2000-01-01\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request2)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(201);

                String request = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"1999-01-02\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(409);
        }

        @Test
        void whenDiscountCardsConflict1YLater() {
                String request2 = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"1999-01-01\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request2)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(201);

                String request = "{"
                        + "\"customerId\": " + customerId.get("id") + ","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"1999-12-31\","
                        + "\"type\": 25"
                        + "}";

                given().header("Content-Type", "application/json")
                        .and()
                        .body(request)
                        .when()
                        .post("/customers/{customerId}/discountcards", customerId.get("id"))
                        .then().statusCode(409);
        }
}
