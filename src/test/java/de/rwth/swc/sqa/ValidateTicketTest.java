package de.rwth.swc.sqa;
import de.rwth.swc.sqa.model.Ticket;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import io.restassured.http.ContentType;
import org.springframework.http.HttpStatus;
import org.threeten.extra.MutableClock;

import java.time.Instant;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class ValidateTicketTest {
    @Autowired
    private MutableClock clock;
    private static final String PATH = "/tickets";
    private static final String DISCOUNTPATH = "/customers/{customerId}/discountcards";
    private static final String CUSTOMERPATH = "/customers";
    private static Response ticketResponse;
    private static final long maxLong = Long.MAX_VALUE;


    private static final String ticketRequestBodyDefault = """
                {
                 "birthdate": "1980-01-01",
                 "validFor": "30d",
                 "validFrom": "2020-10-29T10:38:59"
                }""";
    private static final String ticketRequestBodyZoneA = """
                {
                 "birthdate": "1992-01-01",
                 "validFor": "1h",
                 "validFrom": "2020-10-29T10:38:59",
                 "zone": "A"
                }""";

    private static final String ticketRequestBodyZoneB = """
                {
                 "birthdate": "1992-01-01",
                 "validFor": "1h",
                 "validFrom": "2020-10-29T10:38:59",
                 "zone": "B"
                }""";

    private static final String ticketRequestBodyZoneC = """
                {
                 "birthdate": "1992-01-01",
                 "validFor": "1h",
                 "validFrom": "2020-10-29T10:38:59",
                 "zone": "C"
                }""";

    private static final String ticketRequestBodyDisabledTrue = """
                {
                 "birthdate": "1992-01-01",
                 "validFor": "30d",
                 "validFrom": "2020-10-29T10:38:59",
                 "disabled": true
                }""";

    private static final String ticketRequestBodyStudentTrue = """
                {
                 "birthdate": "1999-01-01",
                 "validFor": "30d",
                 "validFrom": "2020-10-29T10:38:59",
                 "student": true
                }""";

    private static final String ticketRequestBodyDiscountCardTrue = """
                {
                 "birthdate": "1992-01-01",
                 "validFor": "1y",
                 "validFrom": "2020-10-29T10:38:59",
                 "discountCard": 25
                }""";

    private  static final String customerRequest = """
                     {
                     "birthdate": "1992-01-01",
                     "disabled": false
                     }""";

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
        clock.setInstant(Instant.parse("2019-10-29T10:38:59.00Z"));
    }

    @Test
    void ticketIdDoesNotExist() {
        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+maxLong+"\",\n" +
                " \"zone\": \"A\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(403);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2})
    void ticketValidInZoneA(int zoneInt) {
        Ticket.ZoneEnum zoneEnum;
        int status;
        if(zoneInt == 0){
            zoneEnum = Ticket.ZoneEnum.A;
            status = 200;
        } else if (zoneInt == 1){
            zoneEnum = Ticket.ZoneEnum.B;
            status = 403;
        } else {
            zoneEnum = Ticket.ZoneEnum.C;
            status = 403;
        }

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyZoneA)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:36:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \""+ zoneEnum +"\",\n" +
                " \"date\": \"2020-10-29T11:36:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2})
    void ticketValidInZoneB(int zoneInt) {
        Ticket.ZoneEnum zoneEnum;
        int status;
        if(zoneInt == 0){
            zoneEnum = Ticket.ZoneEnum.A;
            status = 200;
        } else if (zoneInt == 1){
            zoneEnum = Ticket.ZoneEnum.B;
            status = 200;
        } else {
            zoneEnum = Ticket.ZoneEnum.C;
            status = 403;
        }

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyZoneB)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:36:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \""+ zoneEnum +"\",\n" +
                " \"date\": \"2020-10-29T11:36:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2})
    void ticketValidInZoneC(int zoneInt) {
        Ticket.ZoneEnum zoneEnum;
        if(zoneInt == 0){
            zoneEnum = Ticket.ZoneEnum.A;
        } else if (zoneInt == 1){
            zoneEnum = Ticket.ZoneEnum.B;
        } else {
            zoneEnum = Ticket.ZoneEnum.C;
        }

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyZoneC)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:36:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \""+ zoneEnum +"\",\n" +
                " \"date\": \"2020-10-29T11:36:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2})
    void ticketDateInvalid(int dateInt) {
        String dateString;
        int status = 403;

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDefault)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        if(dateInt == 1){
            clock.setInstant(Instant.parse("2020-12-29T10:38:59.00Z"));
            dateString = "2020-12-29T10:38:59";
        }
        else {
            clock.setInstant(Instant.parse("2020-08-29T10:38:59.00Z"));
            dateString = "2020-08-29T10:38:59";
        }

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \""+ dateString +"\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4})
    void ticketDateValid(int dateInt) {
        String dateString;
        int status = 200;
        if(dateInt == 0){
            dateString = "1d";
        } else if(dateInt == 1){
            dateString = "30d";
        }
        else if(dateInt == 2){
            dateString = "30d";
        }
        else if(dateInt == 3){
            dateString = "1d";
        }
        else {
            dateString = "1y";
        }

        String ticketRequestBodyDates = "{\n" +
                " \"birthdate\": \"1980-01-01\",\n" +
                " \"validFor\": \""+ dateString +"\",\n" +
                " \"validFrom\": \"2020-10-29T10:38:59\"\n}";

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDates)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T10:39:59.00Z"));
        if (dateInt == 2){
            // set clock
            clock.setInstant(Instant.parse("2020-10-29T10:38:59.00Z"));
        } else if (dateInt == 3){
            // set clock
            clock.setInstant(Instant.parse("2020-10-30T10:38:59.00Z"));
        }

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T10:39:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1})
    void ticketDisabledFalse(int disabledInt) {
        boolean disabledBool;
        int status = 200;
        disabledBool = disabledInt != 0;

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDefault)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": " + disabledBool +",\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1})
    void ticketDisabledTrue(int disabledInt) {
        boolean disabledBool;
        int status;
        if(disabledInt == 0){
            disabledBool = false;
            status = 403;
        } else {
            disabledBool = true;
            status = 200;
        }

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDisabledTrue)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": " + disabledBool +",\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1})
    void ticketStudentFalse(int studentInt) {
        boolean studentBool;
        int status = 200;
        studentBool = studentInt != 0;

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDefault)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": "+ studentBool + "\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2})
    void ticketStudentTrue(int studentInt) {
        boolean studentBool;
        int status;
        if(studentInt == 0){
            studentBool = false;
            status = 403;
        } else if (studentInt == 1){
            studentBool = true;
            status = 200;
        } else {
            studentBool = true;
            status = 403;
        }

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyStudentTrue)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        if(studentInt == 2){
            String ticketRequestBodyStudentTrueBirthdateTooOld = """
                {
                 "birthdate": "1980-01-01",
                 "validFor": "30d",
                 "validFrom": "2020-10-29T10:38:59",
                 "student": true
                }""";

            ticketResponse = given().header("Content-type", "application/json")
                    .and().body(ticketRequestBodyStudentTrueBirthdateTooOld)
                    .when().post(PATH)
                    .then().contentType(ContentType.JSON).extract().response();
            ticketAsString = ticketResponse.asString();
            ticketAsMap = from(ticketAsString).get("");

            if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}
        }

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": "+ studentBool + "\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(status);
    }

    @Test
    void ticketDiscountFalseRequestFalse() {
        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDefault)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(200);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1})
    void ticketDiscountFalseRequestProvidesId(int discountInt) {
        Response customerResponse = given().header("Content-type", "application/json")
                .and().body(customerRequest)
                .when().post(CUSTOMERPATH)
                .then().contentType(ContentType.JSON).extract().response();
        String CustomerAsString = customerResponse.asString();
        Map<String, ?> customerAsMap = from(CustomerAsString).get("");

        if (customerAsMap.getOrDefault("id", null) == null){fail("AddCustomer did not return an id.");}

        String discountCardRequestBody = "{\n" +
                " \"customerId\": \"" + customerAsMap.get("id") + "\",\n" +
                " \"type\": 25,\n" +
                " \"validFrom\": \"2020-10-27\",\n" +
                " \"validFor\": \"1y\"\n}";

        Response discountCardResponse = given().header("Content-type", "application/json")
                .and().body(discountCardRequestBody)
                .when().post(DISCOUNTPATH, customerAsMap.get("id"))
                .then().contentType(ContentType.JSON).extract().response();
        String discountCardAsString = discountCardResponse.asString();
        Map<String, ?> discountCardAsMap = from(discountCardAsString).get("");

        if (discountCardAsMap.getOrDefault("id", null) == null){fail("AddDiscountCardToCustomer did not return an id.");}

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDefault)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String,?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        long discountLong;
        String discountString = "" + discountCardAsMap.get("id");
        if(discountInt == 0){
            discountLong = Long.parseLong(discountString);
        } else {
            discountLong = maxLong;
        }

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \""+ticketAsMap.get("id")+"\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"discountCardId\":" + discountLong + ",\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(403);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    void ticketDiscountTrueRequestProvidesID(int discountInt) {
        Response customerResponse = given().header("Content-type", "application/json")
                .and().body(customerRequest)
                .when().post(CUSTOMERPATH)
                .then().contentType(ContentType.JSON).extract().response();
        String CustomerAsString = customerResponse.asString();
        Map<String, ?> customerAsMap = from(CustomerAsString).get("");

        if (customerAsMap.getOrDefault("id", null) == null){fail("AddCustomer did not return an id.");}

        String discountCardRequestBody = "{\n" +
                    " \"customerId\": \"" + customerAsMap.get("id") + "\",\n" +
                    " \"type\": 25,\n" +
                    " \"validFrom\": \"2020-10-29\",\n" +
                    " \"validFor\": \"1y\"\n}";

        Response discountCardResponse = given().header("Content-type", "application/json")
                .and().body(discountCardRequestBody)
                .when().post(DISCOUNTPATH, customerAsMap.get("id"))
                .then().contentType(ContentType.JSON).extract().response();
        String discountCardAsString = discountCardResponse.asString();
        Map<String, ?> discountCardAsMap = from(discountCardAsString).get("");

        if (discountCardAsMap.getOrDefault("id", null) == null){fail("AddDiscountCardToCustomer did not return an id.");}

        ticketResponse = given().header("Content-type", "application/json")
                    .and().body(ticketRequestBodyDiscountCardTrue)
                    .when().post(PATH)
                    .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String, ?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-30T11:35:59.00Z"));

        long discountLong;
        int status;
        String discountString = "" + discountCardAsMap.get("id");
        if(discountInt == 0){
            discountLong = Long.parseLong(discountString);
            status = 200;
        }
        else if(discountInt == 1){
            discountLong = Long.parseLong(discountString);
            status = 200;
            // set clock
            clock.setInstant(Instant.parse("2020-10-29T11:35:59.00Z"));
        }
        else if(discountInt == 2){
            discountLong = Long.parseLong(discountString);
            status = 200;
            // set clock
            clock.setInstant(Instant.parse("2021-10-28T00:00:00.00Z"));
        }else {
            discountLong = maxLong;
            status = 403;
        }

        String ticketValidationRequestBody = "{\n" +
                    " \"ticketId\": \"" + ticketAsMap.get("id") + "\",\n" +
                    " \"zone\": \"C\",\n" +
                    " \"date\": \"2020-10-29T11:35:59\",\n" +
                    " \"disabled\": false,\n" +
                    " \"discountCardId\":" + discountLong + ",\n" +
                    " \"student\": false\n}";

        given().header("Content-type", "application/json")
                    .and().body(ticketValidationRequestBody)
                    .when().post(PATH + "/validate")
                    .then().statusCode(status);
    }

    @Test
    void ticketDiscountTrueRequestNoId() {
        Response customerResponse = given().header("Content-type", "application/json")
                .and().body(customerRequest)
                .when().post(CUSTOMERPATH)
                .then().contentType(ContentType.JSON).extract().response();
        String CustomerAsString = customerResponse.asString();
        Map<String, ?> customerAsMap = from(CustomerAsString).get("");

        if (customerAsMap.getOrDefault("id", null) == null){fail("AddCustomer did not return an id.");}

        String discountCardRequestBody = "{\n" +
                " \"customerId\": \"" + customerAsMap.get("id") + "\",\n" +
                " \"type\": 25,\n" +
                " \"validFrom\": \"2020-10-27\",\n" +
                " \"validFor\": \"1y\"\n}";

        Response discountCardResponse = given().header("Content-type", "application/json")
                .and().body(discountCardRequestBody)
                .when().post(DISCOUNTPATH, customerAsMap.get("id"))
                .then().contentType(ContentType.JSON).extract().response();
        String discountCardAsString = discountCardResponse.asString();
        Map<String, ?> discountCardAsMap = from(discountCardAsString).get("");

        if (discountCardAsMap.getOrDefault("id", null) == null){fail("AddDiscountCardToCustomer did not return an id.");}

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDiscountCardTrue)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String, ?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \"" + ticketAsMap.get("id") + "\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(200);
    }

    @Test
    void ticketDiscountTrueDiscountCardDateInvalidExpired() {
        Response customerResponse = given().header("Content-type", "application/json")
                .and().body(customerRequest)
                .when().post(CUSTOMERPATH)
                .then().contentType(ContentType.JSON).extract().response();
        String CustomerAsString = customerResponse.asString();
        Map<String, ?> customerAsMap = from(CustomerAsString).get("");

        if (customerAsMap.getOrDefault("id", null) == null){fail("AddCustomer did not return an id.");}

        String discountCardRequestBody = "{\n" +
                " \"customerId\": \"" + customerAsMap.get("id") + "\",\n" +
                " \"type\": 25,\n" +
                " \"validFrom\": \"2018-10-27\",\n" +
                " \"validFor\": \"1y\"\n}";

        Response discountCardResponse = given().header("Content-type", "application/json")
                .and().body(discountCardRequestBody)
                .when().post(DISCOUNTPATH, customerAsMap.get("id"))
                .then().contentType(ContentType.JSON).extract().response();
        String discountCardAsString = discountCardResponse.asString();
        Map<String, ?> discountCardAsMap = from(discountCardAsString).get("");

        if (discountCardAsMap.getOrDefault("id", null) == null){fail("AddDiscountCardToCustomer did not return an id.");}

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDiscountCardTrue)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String, ?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \"" + ticketAsMap.get("id") + "\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"discountCardId\":" + discountCardAsMap.get("id") + ",\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(403);
    }

    @Test
    void ticketDiscountTrueDiscountCardDateTooEarly() {
        Response customerResponse = given().header("Content-type", "application/json")
                .and().body(customerRequest)
                .when().post(CUSTOMERPATH)
                .then().contentType(ContentType.JSON).extract().response();
        String CustomerAsString = customerResponse.asString();
        Map<String, ?> customerAsMap = from(CustomerAsString).get("");

        if (customerAsMap.getOrDefault("id", null) == null){fail("AddCustomer did not return an id.");}

        String discountCardRequestBody = "{\n" +
                " \"customerId\": \"" + customerAsMap.get("id") + "\",\n" +
                " \"type\": 25,\n" +
                " \"validFrom\": \"2022-10-27\",\n" +
                " \"validFor\": \"30d\"\n}";

        Response discountCardResponse = given().header("Content-type", "application/json")
                .and().body(discountCardRequestBody)
                .when().post(DISCOUNTPATH, customerAsMap.get("id"))
                .then().contentType(ContentType.JSON).extract().response();
        String discountCardAsString = discountCardResponse.asString();
        Map<String, ?> discountCardAsMap = from(discountCardAsString).get("");

        if (discountCardAsMap.getOrDefault("id", null) == null){fail("AddDiscountCardToCustomer did not return an id.");}

        ticketResponse = given().header("Content-type", "application/json")
                .and().body(ticketRequestBodyDiscountCardTrue)
                .when().post(PATH)
                .then().contentType(ContentType.JSON).extract().response();
        String ticketAsString = ticketResponse.asString();
        Map<String, ?> ticketAsMap = from(ticketAsString).get("");

        if (ticketAsMap.getOrDefault("id", null) == null){fail("BuyTicket did not return an id.");}

        // set clock
        clock.setInstant(Instant.parse("2020-10-29T11:38:59.00Z"));

        String ticketValidationRequestBody = "{\n" +
                " \"ticketId\": \"" + ticketAsMap.get("id") + "\",\n" +
                " \"zone\": \"C\",\n" +
                " \"date\": \"2020-10-29T11:38:59\",\n" +
                " \"disabled\": false,\n" +
                " \"discountCardId\":" + discountCardAsMap.get("id") + ",\n" +
                " \"student\": false\n}";

        given().header("Content-type", "application/json")
                .and().body(ticketValidationRequestBody)
                .when().post(PATH + "/validate")
                .then().statusCode(403);
    }
}
