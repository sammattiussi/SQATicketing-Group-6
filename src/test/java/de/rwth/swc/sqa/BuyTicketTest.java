package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.threeten.extra.MutableClock;

import java.time.Instant;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class BuyTicketTest {
    @Autowired
    private MutableClock clock;
    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
        clock.setInstant(Instant.parse("2020-01-01T00:00:00.00Z"));
    }

    @Test
    void defaultValueForDefaultFieldsTest() {
        String request =
                "{"
                + "\"birthdate\": \"1995-01-01\","
                + "\"validFor\": \"1d\","
                + "\"validFrom\": \"2020-10-29T10:38:59\""
                + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void valueForDefaultFieldsTest(){
        String request =
                "{"
                + "\"birthdate\": \"1970-01-01\","
                + "\"validFor\": \"1d\","
                + "\"validFrom\": \"2020-10-29T10:38:59\","
                + "\"disabled\": false,"
                + "\"student\": false"
                + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void noValueForValidFromTest(){
        String request =
                "{"
                + "\"birthdate\": \"1970-01-01\","
                + "\"validFor\": \"1d\""
                + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void invalidValidFromTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1999-01-01\","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"2019-10-29T10:38:59\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void invalidValueForValidFromTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void invalidValueForBirthdateTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1970\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void validZoneATest(){
        String request =
                "{"
                + "\"birthdate\": \"1995-01-01\","
                + "\"validFor\": \"1h\","
                + "\"validFrom\": \"2020-10-29T10:38:59\","
                + "\"zone\" : \"A\""
                + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }
    @Test
    void validZoneBTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1995-01-01\","
                        + "\"validFor\": \"1h\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"zone\" : \"B\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }
    @Test
    void validZoneCTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1995-01-01\","
                        + "\"validFor\": \"1h\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"zone\" : \"C\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void invalidZoneATest(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"zone\" : \"A\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }
    @Test
    void invalidZoneBTest(){
            String request =
                    "{"
                            + "\"birthdate\": \"1970-01-01\","
                            + "\"validFor\": \"30d\","
                            + "\"validFrom\": \"2020-10-29T10:38:59\","
                            + "\"zone\" : \"A\""
                            + "}";

            given().header("Content-Type","application/json")
                    .and()
                    .body(request)
                    .when()
                    .post("/tickets")
                    .then().statusCode(400);
    }
    @Test
    void invalidZoneCTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"zone\" : \"C\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void invalidZoneEmptyTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1h\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void validZoneEmptyTest1d(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void validZoneEmptyTest30d(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void validZoneEmptyTest1y(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\""
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void validStudentTicket30dTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1999-01-01\","
                        + "\"validFor\": \"30d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"student\": true"
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void validStudentTicket1yTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1999-01-01\","
                        + "\"validFor\": \"1y\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"student\": true"
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void invalidStudentTicketWrongValidFor1hTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1999-01-01\","
                        + "\"validFor\": \"1h\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"zone\" : \"C\","
                        + "\"student\": true"
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void invalidStudentTicketWrongValidFor1dTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1999-01-01\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"zone\" : \"C\","
                        + "\"student\": true"
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void invalidDiscountCardTest(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"discountCard\": 10"
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(400);
    }

    @Test
    void validDiscountCard25Test(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"discountCard\": 25"
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }

    @Test
    void validDiscountCard50Test(){
        String request =
                "{"
                        + "\"birthdate\": \"1970-01-01\","
                        + "\"validFor\": \"1d\","
                        + "\"validFrom\": \"2020-10-29T10:38:59\","
                        + "\"discountCard\": 50"
                        + "}";

        given().header("Content-Type","application/json")
                .and()
                .body(request)
                .when()
                .post("/tickets")
                .then().statusCode(201);
    }
}