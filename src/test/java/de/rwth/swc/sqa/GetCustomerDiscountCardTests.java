package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.threeten.extra.MutableClock;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class GetCustomerDiscountCardTests {
    private static final String PATH = "/customers/{customerId}/discountcards";
    private static final String CUSTOMER_PATH = "/customers";

    private long customerId;
    private long discountCardId;
    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {

        RestAssured.port = port;
        final String customerRequestBody = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        customerId = Long.parseLong(given().header("Content-type", "application/json")
                .and()
                .body(customerRequestBody)
                .when()
                .post(CUSTOMER_PATH).then().extract().path("id").toString());

        final String discountCardRequestBody = "{"
                + "\"customerId\": "+customerId+","
                + "\"validFor\": \"30d\","
                + "\"validFrom\": \"1992-01-01\","
                + "\"type\": 25"
                + "}";
        discountCardId = Long.parseLong(given().header("Content-type", "application/json")
                .and()
                .body(discountCardRequestBody)
                .when().pathParam("customerId", customerId)
                .post(PATH).then().extract().path("id").toString());
    }

    @Test
    void testStatusNotFoundWithEmptyCustomerId() {
        /*
         * Checks if the customer id is empty, hit the wrong endpoint and return status
         * 404
         */
        given().pathParam("customerId", "").when().get(PATH).then().statusCode(404);
    }

    @Test
    void testStatusBadRequestWithWrongCustomerIdType() {
        /*
         * Checks if the customer has wrong customerId type for example string passed
         * instead of a number return 400
         */
        given().pathParam("customerId", 'a').when().get(PATH).then().statusCode(400);
    }

    @Test
    void testStatusNotFoundWithCorrectCustomerId() {
        /* Checks if the customer is not found then status code is 404 */
        given().pathParam("customerId", Long.MAX_VALUE).when().get(PATH).then().statusCode(404);
    }

    @Test
    void testStatusNotFoundWithCorrectCustomerIdNoDiscountCards() {
        /* Checks if the customer has no discount cards then status code is 404 */
        given().pathParam("customerId", Long.MAX_VALUE).when().get(PATH).then().statusCode(404);
    }

    @Test
    void testStatusOkWithCorrectCustomerId() {
        /* Checks if the status is 200 */
        given().pathParam("customerId", customerId).when().get(PATH).then().statusCode(200);
    }

    @Test
    void testStatusOkWithCorrectCustomerIdAndResponseCorrectJSONFormat() {
        /* Checks if the response json has all the required attributes */
        given().pathParam("customerId", customerId).when().get(PATH).then().statusCode(200)
                .assertThat().body(containsString("customerId"))
                .assertThat().body(containsString("id"))
                .assertThat().body(containsString("validFrom"))
                .assertThat().body(containsString("type"))
                .assertThat().body(containsString("validFor"));
    }

    @Test
    void testStatusOkWithCorrectCustomerIdAndMatchingResponseCustomerId() {
        /* Checks if the customer ID supplied is returned within the discount cards */
        given()
                .pathParam("customerId", customerId)
                .when()
                .get(PATH)
                .then()
                .statusCode(200)
                .assertThat()
                .extract()
                .path("customerId")
                .equals(Long.toString(customerId));
    }

    @Test
    void testStatusOkWithCorrectCustomerIdAndMatchingDiscountCardId() {
        /* Checks if the customer ID supplied is returned within the discount cards */
        given()
                .pathParam("customerId", customerId)
                .when()
                .get(PATH)
                .then()
                .statusCode(200)
                .assertThat()
                .extract()
                .path("id")
                .equals(Long.toString(discountCardId));
    }
}
